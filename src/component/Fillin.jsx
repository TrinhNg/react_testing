import React, { Component } from "react";
import { Box } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

class Fillin extends Component {
  render() {
    return (
      <Box>
        <TextField id="standard-basic" label="Standard" />
        <TextField id="standard-basic" label="Filled" />
        <TextField id="standard-basic" label="Outlined" />
      </Box>
    );
  }
}

export default Fillin;
