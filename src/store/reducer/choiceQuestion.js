const testingOnline = {
  question: [],
};
const testing = (state = testingOnline, action) => {
  switch (action.type) {
    case "SET_COURSE":
      state.question = { ...action.payload };

      return { ...state };
    default:
      return { ...state };
  }
};
export default testing;
