import "./App.css";
import { Box, Typography } from "@material-ui/core";
import Home from "./component/Home";

function App() {
  return (
    <Box color="text.primary" marginTop="20px">
      <Typography variant="h1" component="center">
        Online Testing
      </Typography>
      <Home />
    </Box>
  );
}

export default App;
