import axios from "axios";

export const fetchCourses = async (dispatch) => {
  try {
    const res = await axios({
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions",
      method: "GET",
    });
    dispatch({
      type: "SET_COURSE",
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};
